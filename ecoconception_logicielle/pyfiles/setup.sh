#!/bin/bash

# Use US Locale:
export LANG=C

## resolve folder of this script, following all symlinks:
## http://stackoverflow.com/questions/59895/can-a-bash-script-tell-what-directory-its-stored-in
SCRIPT_SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SCRIPT_SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
  SCRIPT_DIR="$( cd -P "$( dirname "$SCRIPT_SOURCE" )" && pwd )"
  SCRIPT_SOURCE="$(readlink "$SCRIPT_SOURCE")"
  # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
  [[ $SCRIPT_SOURCE != /* ]] && SCRIPT_SOURCE="$SCRIPT_DIR/$SCRIPT_SOURCE"
done
readonly SCRIPT_DIR="$( cd -P "$( dirname "$SCRIPT_SOURCE" )" && pwd )"

source $SCRIPT_DIR/env.sh


# Setup commands:
set -ex

# clean:
rm -rf __pycache__ deprecated/__pycache__

if [ "$1" == "clean" ]; then
    exit 0
fi

# build:
# update Python env:
pip3 install --upgrade pip setuptools wheel # separate install needed

pip3 install numpy
pip3 install transonic pythran
pip3 install numba matplotlib


# test transonic and numba to pre-compile jit:
echo "Running JIT 1Dpython_transonic"
$SCRIPT_DIR/1Dpython_transonic

echo "Running JIT 1Dpython_numba"
$SCRIPT_DIR/1Dpython_numba

