#!/bin/bash

#export JAVA_HOME=/home/bourgesl/apps/jdk-11.0.6+10/
#export GRAALVM_HOME=/home/bourgesl/apps/graalvm-ce-java11-19.3.1/
#export GRAALVM_HOME=/home/bourgesl/apps/graalvm-ee-java11-19.3.1/

echo "Java version:"
java -version

# $GRAALVM_HOME/bin/java -version

# Note: do not exceed 4Gb (4 x Xmx settings) for Rasperry Pi 4:
export JAVA_OPTS="-Xms512m -Xmx512m"
#export JAVA_OPTS="-Xms512m -Xmx512m -verbose:gc"

# disable warmup (jit will be more noticeable):
#export JAVA_OPTS="-Dwi=0 -Dbi=50 $JAVA_OPTS"

echo "JAVA_OPTS : $JAVA_OPTS"

