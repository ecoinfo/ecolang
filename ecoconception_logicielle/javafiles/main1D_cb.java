/*
EcoInfo - Time & energy efficiency of programming languages in HPC
Copyright (C) 2020, CNRS

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
import java.security.AccessController;
import java.util.Locale;

public final class main1D_cb {

    static double[] TDMAsolver(final int M, double a[], double b[], double c[], double d[]) {
        /* Tri Diagonal Matrix Algorithm(a.k.a Thomas algorithm) solver
         TDMA solver, a b c d can be NumPy array type or Python list type.
         refer to http://en.wikipedia.org/wiki/Tridiagonal_matrix_algorithm

            Sub TriDiagonal_Matrix_Algorithm(N%, A#(), B#(), C#(), D#(), X#())
                Dim i%, W#
                For i = 2 To N
                    W = A(i) / B(i - 1)
                    B(i) = B(i) - W * C(i - 1)
                    D(i) = D(i) - W * D(i - 1)
                Next i
                X(N) = D(N) / B(N)
                For i = N - 1 To 1 Step -1
                    X(i) = (D(i) - C(i) * X(i + 1)) / B(i)
                Next i
            End Sub

        See https://www.harrisgeospatial.com/docs/TRISOL.html
         */
        final double[] bc = new double[M];
        final double[] dc = new double[M];

        System.arraycopy(b, 0, bc, 0, M);
        System.arraycopy(d, 0, dc, 0, M);

        for (int it = 1; it < M; it++) {
            double mc = a[it - 1] / bc[it - 1];
            bc[it] = bc[it] - mc *  c[it - 1];
            dc[it] = dc[it] - mc * dc[it - 1];
        }
        // bc stores the results x:
        bc[M - 1] = dc[M - 1] / bc[M - 1];

        for (int it = M - 2; it >= 0; it--) {
            bc[it] = (dc[it] - c[it] * bc[it + 1]) / bc[it];
        }
        return bc;
    }

    static double RMS_E(final int M, double X1[], double X2[]) {
        double rms = 0.0;
        for (int it = 0; it < M; it++) {
            rms += (X1[it] - X2[it]) * (X1[it] - X2[it]);
        }
        return Math.sqrt(rms / M);
    }

    public static int solve(final int M, final int dbg) {
        double Lx, dx, dpdx, Re, Gammaa, c0, dt, Fe, De, Fw, eps, Dw, aE, aW, aP;
        double rms_u, last_rms, min_step;
        int casee, bcnorth, bcsouth, maxiter, k, it;

        final long start = System.nanoTime();

        //     INPUT PARAMETERS
        //
        // Domain length
        Lx = 1.0;

        // Grid sizes
        dx = Lx / (M - 1);

        // Pressure gradient
        dpdx = 1.0;

        // Reynolds number: Re = U L / nu
        Re = 1.0;

        // Diffusivity
        Gammaa = 1.0 / Re;

        // Velocity
        c0 = 0.1;

        // Time step:
        dt = 0.1;

        // Configuration
        casee = 0; //'diffusion'
        //casee = 1 //'convection'

        //
        //     INITIALISATION
        //
        final double[] Aua = new double[M - 1];
        final double[] Auc = new double[M - 1];

        final double[] Aub = new double[M];
        final double[] Aud = new double[M];
        final double[] uold = new double[M];
        double[] u = null;

        if (casee == 0) {
            // Pressure gradient
            dpdx = 1.0;
            // Velocity
            c0 = 0.0;
            // Diffusivity
            Gammaa = 1.0 / Re;
            // Time step:
            dt = 0.1;
            // Maximum number of iterations
            maxiter = 1000;
        } else {
            System.out.printf("Invalid case = %d \n", casee);
            return 1; // failure
        }

        // Precision
        eps = 1e-9;


        // Print initial conditions:
        if (dbg == 1) {
            System.out.printf("case = %d \n", casee);
            System.out.printf("M = %d \n", M);
            System.out.printf("dx = %.17g \n", dx);
            System.out.printf("dt = %.17g \n", dt);
            System.out.printf("eps = %.17g \n", eps);
        }

        // boundary conditions
        bcnorth = 0;
        bcsouth = 0;

        // Initialisation of variables
        // implicit in java

        //
        // Convection fluxes
        //
        Fe = c0;
        Fw = c0;
        //
        // Diffusion fluxes
        //
        De = Gammaa / dx;
        Dw = Gammaa / dx;
        //
        // Compute system coefficients
        //
        aE = De + Math.max(-Fe, 0.0);
        aW = Dw + Math.max(Fw, 0.0);
        aP = aE + aW + Fe - Fw;
        //
        // Assemble matrix coefficients
        //
        for (it = 0; it < M - 1; it++) {
            Aua[it] = -aW;
            Auc[it] = -aE;

        }
        for (it = 1; it < M - 1; it++) {
            Aub[it] = dx / dt + aP;
        }
        // Min step check:
        last_rms = 1.0;
        min_step = eps / 10.0;

        // First iteration
        k = 0;
        while (k < maxiter) {
            //Predictor step (input : c0, Gamma, dx, dt, uold, dpdx; output : Aua, Aub, Auc, Aud)
            for (it = 0; it < M; it++) {
                Aud[it] = dx / dt * uold[it] + dpdx * dx;
            }
            // Boundary conditions
            // North boundary
            if (bcnorth == 0)
            { // Dirichlet
                Aub[M - 1] = 1.0;
                Aud[M - 1] = 0.0;
            }
            else if (bcnorth == 1)
            { // Neumann
                Aua[M - 2] = -1.0;
                Aub[M - 1] = 1.0;
                Aud[M - 1] = 0.0;
            }
            // South boundary
            if (bcsouth == 0)
            { // Dirichlet
                Aub[0] = 1.0;
                Aud[0] = 0.0;
            }
            else if (bcsouth == 1)
            { // Neumann
                Auc[0] = -1.0;
                Aub[0] = 1.0;
                Aud[0] = 0.0;
            }

            // Solve algebraic system
            u = TDMAsolver(M, Aua, Aub, Auc, Aud);

            // Compute rms error at the end of each time step
            rms_u = RMS_E(M, u, uold);

            if (rms_u < eps) {
                if (dbg == 1) {
                    System.out.printf("The simulation has converged in %d iterations\n", k);
                }
                break;
            } else if (Math.abs(last_rms - rms_u) < min_step) {
                if (dbg == 1) {
                    System.out.printf("The simulation convergence is limited to %.17g in %d iterations\n", rms_u, k);
                }
                break;
            } else {
                if (dbg == 1) {
                    System.out.printf("k=%d : RMS U = %.17g\n", k, rms_u);
                }
            }
            // Copy last result:
            System.arraycopy(u, 0, uold, 0, M);
            k += 1;
            last_rms = rms_u;
        }

        final long stop = System.nanoTime();

        if (dbg == 1) {
            System.out.printf("|Bench| Time (s): %f \n", 1e-9 * (stop - start));  // Time in seconds, e.g. 5.38091952400282

            System.out.printf("result samples:\n");
            final int step = M / 10;
            for (k = 0; k < M; k += step) {
                System.out.printf("u[%d] = %.17g \n", k, u[k]);
            }
        }
        return 0;
    }

    public static void main(String[] args) {
        // Set the default locale to en-US locale (for Numerical Fields "." ",")
        Locale.setDefault(Locale.US);

        final int WI = getInteger("wi", 100, 0, 10000);
        final int WM = 64 * 1024;
        final int BI = getInteger("bi", 10, 1, 10000);
        final int BM = 1 * 2048 * 2048;
        int i;

        System.out.printf("|Bench| WARMUP %d iterations [len = %d]---\n", WI, WM);
        System.out.printf("|Bench| WARMUP Start: %d \n", System.currentTimeMillis() / 1000l);

        for (i = 0; i < WI; i++) {
            solve(WM, 0); /* hide logs */
        }

        System.out.printf("|Bench| WARMUP Stop: %d \n", System.currentTimeMillis() / 1000l);

        try {
            Thread.sleep(3000);
        } catch (final InterruptedException ie) {
            // no-op
        }

        System.out.printf("|Bench| BENCHMARK %d iterations [%d]---\n", BI, BM);
        System.out.printf("|Bench| BENCHMARK Start: %d \n", System.currentTimeMillis() / 1000l);

        for (i = 0; i < BI; i++) {
            solve(BM, 1); /* show logs */
        }

        System.out.printf("|Bench| BENCHMARK Stop: %d \n", System.currentTimeMillis() / 1000l);
    }
    
    static int getInteger(final String key, final int def,
                          final int min, final int max)
    {
        final String property = System.getProperty(key);

        int value = def;
        if (property != null) {
            try {
                value = Integer.decode(property);
            } catch (NumberFormatException e) {
                System.err.println("Invalid integer value for " + key + " = " + property);
            }
        }

        // check for invalid values
        if ((value < min) || (value > max)) {
            System.err.println("Invalid value for " + key + " = " + value
                    + "; expected value in range[" + min + ", " + max + "] !");
            value = def;
        }
        return value;
    }
}

