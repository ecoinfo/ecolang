#!/bin/bash

# Use US Locale:
export LANG=C


echo "clean-all: start ..."

# C:
./clean-bench.sh cfiles/1DC

# Fortran:
./clean-bench.sh ffiles/1Dfortran

# Java
./clean-bench.sh javafiles/1Djava

# Julia:
./clean-bench.sh jlfiles/1Djulia

# Python:
./clean-bench.sh pyfiles/1Dpython_numpy

# Rust:
./clean-bench.sh rustfiles/1Drust

# Go:
./clean-bench.sh gofiles/1Dgo

echo "clean-all: done."

