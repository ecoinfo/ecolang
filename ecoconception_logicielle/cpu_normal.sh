#!/bin/bash
# Needs sudo

# Use US Locale:
export LANG=C

echo "enable C-states ..."
cpupower -c all idle-set -E

# restore turbo-boost
echo 0 > /sys/devices/system/cpu/intel_pstate/no_turbo

MIN=`cat /sys/devices/system/cpu/cpu0/cpufreq/cpuinfo_min_freq`
MAX=`cat /sys/devices/system/cpu/cpu0/cpufreq/cpuinfo_max_freq`

MIN=$((MIN / 1000))
MAX=$((MAX / 1000))

#echo "MIN: $MIN"
#echo "MAX: $MAX"

echo "set CPU frequency in ($MIN - $MAX) and governor to powersave ..."
cpupower -c all frequency-set --min ${MIN}MHz --max ${MAX}MHz -g powersave 
cpupower -c all frequency-info

# set perf bias
cpupower -c all set -b 6
cpupower -c all info

