Fortran version:
GNU Fortran (Raspbian 8.3.0-6+rpi1) 8.3.0
Copyright (C) 2018 Free Software Foundation, Inc.
This is free software; see the source for copying conditions.  There is NO
warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

+ rm -f main1D_cb.o main1D_cb_opt.o func.mod 1Dfortran 1Dfortran_opt
+ gfortran -Wall -O3 -c main1D_cb.f90
+ gfortran -Wall -O3 -o 1Dfortran main1D_cb.o
+ gfortran -Wall -O3 -c main1D_cb_opt.f90
+ gfortran -Wall -O3 -o 1Dfortran_opt main1D_cb_opt.o
