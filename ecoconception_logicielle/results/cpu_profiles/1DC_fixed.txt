./run-bench.sh cfiles/1DC
---------------------------------------
Run benchmarks on command 'cfiles/1DC' on 4 CPU cores
---
Store results in ./results/1DC/2020_02_17T15_11_41
Start vmstat (1s) in background (CPU 0)...
---
Run Benchmark on 1 CPU (monothread) ... 'cfiles/1DC'
Run process (CPU 1)...
Results (monothread):
Time in seconds = 3.268691 
Time in seconds = 3.268809 
Time in seconds = 3.268137 
Time in seconds = 3.270206 
Time in seconds = 3.270382 
Time in seconds = 3.272855 
Time in seconds = 3.270505 
Time in seconds = 3.268361 
Time in seconds = 3.266850 
Time in seconds = 3.269510 
---
Run Benchmarks on 4 CPUs (parallel) ... 'cfiles/1DC'
Spawn process 1 (CPU 0))
Spawn process 2 (CPU 1))
Spawn process 3 (CPU 2))
Spawn process 4 (CPU 3))
Waiting ...
All Processes terminated.
Results (parallel):
Time in seconds = 3.618917 
Time in seconds = 3.726226 
Time in seconds = 3.655508 
Time in seconds = 3.629367 
Time in seconds = 3.710412 
Time in seconds = 3.651288 
Time in seconds = 3.640245 
Time in seconds = 3.667858 
Time in seconds = 3.669318 
Time in seconds = 3.726904 
Time in seconds = 3.624916 
Time in seconds = 3.716500 
Time in seconds = 3.659835 
Time in seconds = 3.659944 
Time in seconds = 3.693740 
Time in seconds = 3.632325 
Time in seconds = 3.674695 
Time in seconds = 3.718334 
Time in seconds = 3.698669 
Time in seconds = 3.645966 
Time in seconds = 3.601412 
Time in seconds = 3.649214 
Time in seconds = 3.648766 
Time in seconds = 3.614574 
Time in seconds = 3.702849 
Time in seconds = 3.639463 
Time in seconds = 3.621823 
Time in seconds = 3.631283 
Time in seconds = 3.637164 
Time in seconds = 3.624993 
Time in seconds = 3.725382 
Time in seconds = 3.791684 
Time in seconds = 3.659853 
Time in seconds = 3.642946 
Time in seconds = 3.683905 
Time in seconds = 3.683629 
Time in seconds = 3.713896 
Time in seconds = 3.754636 
Time in seconds = 3.737637 
Time in seconds = 3.694934 
---
Done.
./run-bench.sh: line 111:  5717 Terminated              taskset -c 0 vmstat -t 1 2>&1 > ${vmFile}
---------------------------------------

