#!/bin/bash

# Use US Locale:
export LANG=C


echo "env-all: start ..."

# C:
./env-bench.sh cfiles/1DC

# Fortran:
./env-bench.sh ffiles/1Dfortran

# Java
./env-bench.sh javafiles/1Djava

# Julia:
./env-bench.sh jlfiles/1Djulia

# Python:
./env-bench.sh pyfiles/1Dpython_numpy

# Rust:
./env-bench.sh rustfiles/1Drust

# Go:
./env-bench.sh gofiles/1Dgo

echo "env-all: done."

