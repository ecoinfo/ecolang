#!/bin/bash

# TRAP: Do not leave children jobs running if the shell has been cancelled
cleanup_trap() {
    CHILDREN_PIDS=$(jobs -p)
    if [ -n "$CHILDREN_PIDS" ]
    then
        trap - EXIT
        echo -e "SHELL cancelled, stopping $CHILDREN_PIDS"
        # we may try to send only TERM before a pause and a last loop with KILL signal ?
        kill $CHILDREN_PIDS

        echo -e "SHELL cancelled, waiting on $CHILDREN_PIDS"
        # wait for all pids
        for pid in $CHILDREN_PIDS; do
            wait $pid
        done

        CHILDREN_PIDS=$(jobs -p)
        if [ -n "$CHILDREN_PIDS" ]
        then
            echo -e "SHELL cancelled, killing $CHILDREN_PIDS"
            kill -9 $CHILDREN_PIDS
        fi
  fi
}
trap cleanup_trap EXIT


# HERE BEGINS THE SCRIPT

# Use US Locale:
export LANG=C

USE_CPU_POWER=1


if [ -z "$1" ]
  then
    echo "No argument 1 (CMD) supplied"
    exit 1
fi


processId="1D" # all benchmarks contains '1D'

# Check if service is running:
PID=`ps -e | grep "${processId}" | grep -v "grep" | grep -v "\-hpl\.sh" | awk '{print $1}'`
#echo "PID: '${PID}'"

N_PID=`echo "${PID}" | wc -w`

if [ "${N_PID}" -ne "0" ]
then
    echo "Process '${processId}' (PID = ${PID}) is already running ..."
    exit 1
fi


# Initialize CPU:
if [ "$USE_CPU_POWER" -eq "1" ]
then
    echo "adjusting CPU (HT OFF) - requires SUDO"
    sudo ./cpu_ht_off.sh 2>&1 > /dev/null
fi


# Log host env:
./log_env.sh




HOSTNAME=`hostname`
DIRPWD=`pwd`

dir=${DIR}/results/${HOSTNAME}-$(date +"%Y_%m_%d")-hpl
mkdir -p $dir
echo "Store results in $dir"

logExt=txt
vmFile=${dir}/vm_${LABEL}.${logExt}


# Log timestamps (needed by getwatts)
hostname > ${dir}/timestamp
date +%s >> ${dir}/timestamp

echo "---------------------------------------"
echo "Run benchmarks on command '$CMD' on 1 CPU core (cpu governors)"
echo "---"


# PROFILE in [cpu_normal | cpu_fixed | cpu_hpc | cpu_hpc_turbo]
for PROFILE in cpu_normal cpu_fixed cpu_hpc cpu_hpc_turbo
do
    # Initialize CPU:
    echo "adjusting CPU (${PROFILE}) - requires SUDO"
    sudo ./${PROFILE}.sh 2>&1 > ${dir}/cpu_profile.txt


    # Single job:
    echo "---"
    echo "Run Benchmark on 1 CPU (monothread) ... '$CMD'"

    logFile=${dir}/result_${PROFILE}_${LABEL}

    # Log timestamps (needed by getwatts)
    # Start linpack:
    echo "Run process hpl linpack"
    export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/opt/arm/armpl_20.3_gcc-8.2/lib/
    pwd
    cd hpl/hpl-2.2/bin/Linux_GCC/
    date +%s >> ${dir}/timestamp

    mpirun --allow-run-as-root -np 64 ./xhpl > ${logFile}_xhpl.${logExt} 2>&1
    date +%s >> ${dir}/timestamp
    cd ../../../../

    echo "---"
done
   


# Log timestamps (needed by getwatts)
# End:
sleep 5
date +%s >> ${dir}/timestamp



# Restore CPU:
if [ "$USE_CPU_POWER" -eq "1" ]
then
    echo "adjusting CPU (HT ON + normal) - requires SUDO"
    sudo ./cpu_ht_on.sh 2>&1 > /dev/null
    sudo ./cpu_normal.sh 2>&1 > /dev/null
fi

trap - EXIT
echo "Done."
echo "---------------------------------------"

