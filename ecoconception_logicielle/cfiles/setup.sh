#!/bin/bash

# Use US Locale:
export LANG=C

## resolve folder of this script, following all symlinks:
## http://stackoverflow.com/questions/59895/can-a-bash-script-tell-what-directory-its-stored-in
SCRIPT_SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SCRIPT_SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
  SCRIPT_DIR="$( cd -P "$( dirname "$SCRIPT_SOURCE" )" && pwd )"
  SCRIPT_SOURCE="$(readlink "$SCRIPT_SOURCE")"
  # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
  [[ $SCRIPT_SOURCE != /* ]] && SCRIPT_SOURCE="$SCRIPT_DIR/$SCRIPT_SOURCE"
done
readonly SCRIPT_DIR="$( cd -P "$( dirname "$SCRIPT_SOURCE" )" && pwd )"

source $SCRIPT_DIR/env.sh


# Setup commands:
set -ex

# clean:
rm -f 1DC 1DC_opt
rm -f 1DC_clang 1DC_opt_clang

if [ "$1" == "clean" ]; then
    exit 0
fi

# build:
$CC $CCLAGS main1D_cb.c -lm -o 1DC
$CC $CCLAGS main1D_cb_opt.c -lm -o 1DC_opt

