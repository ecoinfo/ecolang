#!/bin/bash

# Use US Locale:
export LANG=C

CC=clang
CCLAGS="-Wall -O3"


echo "CC version:"
$CC --version

echo "CCLAGS : $CCLAGS"


# clean:
rm 1DC_clang 1DC_opt_clang

if [ "$1" == "clean" ]; then
    exit 0
fi

# build:
# Setup commands:
set -ex

$CC $CCLAGS main1D_cb.c -lm -o 1DC_clang
$CC $CCLAGS main1D_cb_opt.c -lm -o 1DC_opt_clang

