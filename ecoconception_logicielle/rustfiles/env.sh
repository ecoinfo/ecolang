#!/bin/bash

export PATH=/opt/rust/bin:$PATH

export RUSTFLAGS="-C target-cpu=native"

echo "Rust version:"
rustc --version

echo "Cargo version:"
cargo --version

echo "RUSTFLAGS: $RUSTFLAGS"

