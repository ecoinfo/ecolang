import process_data

show_plot=False

dir='../results/raspberrypi-2022_01_09/'
host_name="rpi4"
cpu_type="RPi 4B (4G)"
nTh=4.0
use_rust_opt3=True # True supported!

# Power sampling:
pwr_dt=1.0 # 01 value(s) per second

# note: Missing watt data for this platform!
inst_pwr_1T=3.0 # W
inst_pwr_MT=5.1 # W

# violin plot (max time)
time_ylim_1T=22.0
time_ylim_MT=45.0

process_data.process(dir, host_name, cpu_type, nTh,
            load_watt_data=False, pwr_dt=pwr_dt,
            inst_pwr_1T=inst_pwr_1T, inst_pwr_MT=inst_pwr_MT,
            show_plot=show_plot, use_rust_opt3=use_rust_opt3,
            time_ylim_1T=time_ylim_1T, time_ylim_MT=time_ylim_MT)

