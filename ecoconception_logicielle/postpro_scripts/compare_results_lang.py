import os

import matplotlib.colors as mcolors
import matplotlib.lines as mlines
import matplotlib.pyplot as plt
import matplotlib.ticker as mticker
import numpy as np

plt.rc('font', size=28)


# TODO: 2022.05:
# - use all measurements to generate violin plots (real dispersion ?)
# - run 1Drust_opt3 on g5k then remove filter 'skip 1Drust_opt results on g5k' 


# define all langs:
# skip 'python_numpy'
all_langs = ['C', 'fortran', 'java', 'julia', 'python_numba', 'python_transonic', 'go', 'rust']
# contest:
all_langs = ['C', 'fortran', 'julia', 'java', 'python_transonic', 'python_numba', 'go', 'rust']
print(f"all_langs: {all_langs}")

# excluded "sagittaire-6" (opteron too slow) ?
host_names = ["troll1", "neowise10", "pyxis-4", "zbook-15", "rpi4"]

# specific hostnames included for rust/opt:
#rust_opt_host_names =  ["troll1", "neowise10", "pyxis-4", "zbook-15", "rpi4"]
rust_opt_host_names =  ["zbook-15", "rpi4"]
print(f"rust_opt_host_names: {rust_opt_host_names}")

legend_names = {
    "troll1": "Xeon Gold 5218 [32C]",
    "neowise10": "AMD EPYC 7642 [48C]",
    "pyxis-4": "ARM X2 99xx [64C]",
    "sagittaire-6": "AMD Opteron 250 [2C]",
    "zbook-15": "HP ZBook 15 G3 [4C]",
    "rpi4": "Raspberry Pi 4 [4C]"
}

host_colors = {
    "troll1": "C0",
    "neowise10": "C1",
    "pyxis-4": "C2",
    "sagittaire-6": "C3",
    "zbook-15": "C4",
    "rpi4": "C5"
}


cmp_mode='opt' # or 'ref'

# result_keys=['1T-ref', 'MT-ref', '1T-opt', 'MT-opt']
#result_keys = [f"1T-{cmp_mode}", f"MT-{cmp_mode}"]
result_keys = [f"MT-{cmp_mode}"]


# define all variants [ref|opt]:
v_ref = 'ref'
v_opt = 'opt'
all_variants = [v_ref, v_opt]
print(f"all_variants: {all_variants}")

# define all modes [1T|MT]:
m_1T = '1T'
m_MT = 'MT'
all_modes = [v_ref, v_opt]
print(f"all_modes: {all_modes}")

mode_linestyle = {
    m_1T: ':',
    m_MT: '-'
}

lang_markers = {
    'C': {'s': "o", 'sc': 0.6, 'z': 0},
    'fortran': {'s': "^", 'sc': 0.9, 'z': 1},
    'java': {'s': "s", 'sc': 0.5, 'z': 0},
    'julia': {'s': "*", 'sc': 1.2, 'z': 2},
    'python_numpy': {'s': ".", 'sc': 1.0, 'z': 0},
    'python_numba': {'s': "P", 'sc': 0.8, 'z': 3},
    'python_transonic': {'s': "X", 'sc': 0.8, 'z': 4},
    'go': {'s': "D", 'sc': 0.5, 'z': 0},
    'rust': {'s': "H", 'sc': 0.8, 'z': 0}
}
# print(f"lang_markers: {lang_markers}")

loaded_results = {}

# Load process (all results):
for host_name in host_names:
    with open(f"./output/{host_name}-lang.dic", 'r') as f:
        text = f.read()
        loaded_dic = eval(text)
        loaded_results[host_name] = loaded_dic
        loaded_results[host_name]['host_name'] = host_name

# print(f"loaded_results: {loaded_results}")


fig = plt.figure(1, figsize=[25, 15], dpi=100)
axes = plt.axes()
axes.set_xlabel('Lang')
#axes.set_xscale("log")
axes.set_xlim(-1, len(all_langs)*2 - 1)

# Set ticks labels for x-axis

axes.set_xticks([x*2.0 for x in range(len(all_langs))])
axes.set_xticklabels(all_langs, fontsize=23)


axes.set_ylabel('Energy ratio % C lang')
axes.set_ylim(0.9, 1.8)
axes.set_yticks([0.9 + x*0.1 for x in range(10)])

axes.get_yaxis().set_major_formatter(mticker.ScalarFormatter())

first_lang = True

ref_host_result = None
#ref_host_result = loaded_results["troll1"]  # per xeon reference

vp_data = {}

n_w    = 0.5
n_off  = -n_w / 2.0
n_step = n_w / len(loaded_results)

for nn, host_result in enumerate(loaded_results.values()):
    host_name = host_result['host_name']
    host_color = host_colors[host_name]
    
    for result_key in result_keys:
        result = host_result[result_key]
        langs = []
        times = []
        energies = []
                
        # per C lang:
        ref_result = ref_host_result[result_key] if ref_host_result else host_result[result_key]
        ref_result_lang = ref_result['C']

        # print(f"ref_result: {ref_result}")
        # print(f"result: {result}")

        for ii, lang in enumerate(all_langs):
            langs.append(lang)
            
            if not lang in vp_data:
                vp_data[lang] = []
            
            # skip 1Drust_opt results on g5k:
            if lang in result and (cmp_mode != 'opt' or lang != 'rust' or host_name in rust_opt_host_names):
                r = result[lang]
                if ref_result_lang:
                    # times.append(r['T'] / ref_result_lang['T'])
                    times.append(ii*2 + n_off + n_step * nn)
                    energies.append(r['E'] / ref_result_lang['E'])
                    
                    vp_data[lang].append(r['E'] / ref_result_lang['E'])
                else:
                    print("bad")
                    times.append(r['T'])
                    energies.append(r['E'])
            else:
                times.append(np.NAN)
                energies.append(np.NAN)

        # print(f"T: {langs}")
        # print(f"T: {times}")
        # print(f"E: {energies}")

        for ii, lang in enumerate(langs):
            label = lang if first_lang else None
            plt.scatter(times[ii], energies[ii],
                        marker=lang_markers[lang]['s'], s=600 * lang_markers[lang]['sc'],
                        linewidth=2, color=host_color, edgecolors='black', label=label,
                        zorder=10 + lang_markers[lang]['z'])
                
        first_lang = False

        if False:
            # sort for lines:
            zipped_sorted = sorted(zip(times, energies, langs),
                                   key=lambda i: i[0] if not np.isnan(i[0]) else float('+inf'))  # sorted by times
            times, energies, langs = map(list, zip(*zipped_sorted))

            # print(f"T: {langs}")
            # print(f"T: {times}")
            # print(f"E: {energies}")

            label = f"{legend_names[host_name]}" if result['mode'] == m_MT else None
            ls = mode_linestyle[result['mode']]

            plt.plot(times, energies, label=label, color=host_color, linewidth=0.5, linestyle=ls, zorder=0)
        else:
            ii = 0
            lang = 'C'
            # first host:
            label = f"{legend_names[host_name]}" if result['mode'] == m_MT else None
            plt.scatter(times[ii], energies[ii],
                        marker=lang_markers[lang]['s'], s=600 * lang_markers[lang]['sc'],
                        linewidth=2, color=host_color, edgecolors='black', label=label,
                        zorder=0)


# print(f"vp_data: {vp_data}")


for ii, lang in enumerate(all_langs):
    if ii != 0:
        parts = plt.violinplot(vp_data[lang], positions=[ii*2],
                    widths=1.0,
                    showmeans=True, showextrema=True, showmedians=True)
                   
        for pc in parts['bodies']:
            pc.set_facecolor('gray')
            pc.set_edgecolor('darkgray')
                   
        parts['cmins'].set_color('black')
        parts['cmaxes'].set_color('black')
        parts['cbars'].set_color('black')
        parts['cmedians'].set_color('g')
        parts['cmedians'].set_linewidth(2)        
        parts['cmeans'].set_color('b')
        parts['cmeans'].set_linewidth(2)        



mode = 'Optimized' if cmp_mode == 'opt' else 'Reference'

plt.title(f"Energy efficiency vs Lang [{mode}] [MT]", pad=15)
plt.grid(True, which='both')

h, l = axes.get_legend_handles_labels()
#first_len=len(host_names)
first_len=len(langs)

# host names:
first_legend = axes.legend(h[first_len:], l[first_len:], loc='upper left')
# langs
sec_legend = axes.legend(h[:first_len], l[:first_len], loc='upper center') #'center right'

plt.gca().add_artist(first_legend)
plt.gca().add_artist(sec_legend)

if False:
    # 1T / MT:
    line_1T = mlines.Line2D([], [], color='black', linestyle=mode_linestyle[m_1T], linewidth=5, label='mono-thread [1T]')
    line_MT = mlines.Line2D([], [], color='black', linestyle=mode_linestyle[m_MT], linewidth=5, label='multi-thread [MT]')
    axes.legend(handles=[line_1T, line_MT], loc='upper center')

plt.subplots_adjust(top=0.95, bottom=0.07, left=0.075, right=0.98)

plt.savefig("ecolang_compare_lang.png", dpi=200)
plt.show()

