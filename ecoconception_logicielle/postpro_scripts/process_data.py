import os
import numpy as np
import matplotlib.pyplot as plt

plt.rc('font', size=16)

# define all langs:
lang_C = 'C'
all_langs = [lang_C, 'fortran', 'java', 'julia', 'python_numpy', 'python_numba', 'python_transonic', 'go', 'rust']
print(f"all_langs: {all_langs}")

# define all variants [ref|opt]:
v_ref = 'ref'
v_opt = 'opt'
all_variants = [v_ref, v_opt]
# print(f"all_variants: {all_variants}")

# define all modes [1T|MT]:
m_1T = '1T'
m_MT = 'MT'
all_modes = [v_ref, v_opt]
# print(f"all_modes: {all_modes}")

# define result keys [f|r]:
r_f = 'f'
r_r = 'r'


def process(dir, host_name, cpu_type, nTh,
            load_watt_data=True, pwr_dt=1.0,
            inst_pwr_1T=np.nan, inst_pwr_MT=np.nan,
            show_plot=False, use_rust_opt3=False,
            time_ylim_1T=20, time_ylim_MT=20):

    # Scan result folder to get all potential result files stored in allDict[lang][ref|opt][1T|MT]:
    allDict = {}

    for (dirpath, dirnames, filenames) in os.walk(dir):
        # sort dirs and files
        dirnames.sort()
        filenames.sort()

        for file in filenames:
            # parse filenames like 'result_1D<lang>(_opt)_<single|core_NN>.txt
            if 'result_1D' in file:
                # print(f"file: {file}")
                istart = len('result_1D')

                # identify lang:
                lang = None
                for la in all_langs:
                    if la in file[istart:-1]:
                        lang = la
                        break

                if lang is None:
                    print(f"No lang found in '{file}'")
                    continue

                istart += len(lang)
                iend = file.index("_", istart)
                # has "_opt_" ?
                suffix = file[iend: iend + 5]

                if use_rust_opt3 and lang == "rust" and suffix == "_opt_":
                    # print("skip _opt_ for rust")
                    continue

                pattern = "_opt3" if (use_rust_opt3 and lang == "rust") else "_opt_"
                v = v_opt if (suffix == pattern) else v_ref

                if v == v_opt:
                    istart = file.index("_", iend + 1)
                else:
                    istart = iend
                iend = file.index(".", istart)
                variant = file[istart + 1: iend]

                # 1T or MT
                m = m_1T if 'single' in variant else m_MT

                if not lang in allDict:
                    allDict[lang] = {v_ref: {m_1T: {r_f: [], r_r: []}, m_MT: {r_f: [], r_r: []}},
                                     v_opt: {m_1T: {r_f: [], r_r: []}, m_MT: {r_f: [], r_r: []}}}

                parent = allDict[lang][v][m][r_f]

                if parent is None:
                    print(f"No parent for {lang} {v} {m}")
                    continue

                parent.append(os.path.join(dirpath, file))

    # print(f"allDict: {allDict}")

    # Load watt data:
    if load_watt_data:
        with open(dir + '/powerW.out', 'rb') as f:
            timeTotal = np.load(f)

        # print("timeTotal dims: ", np.shape(timeTotal))
        # timeTotal[timestamp][0 = Watt, 1 = Power integrated]

        plt.plot(timeTotal[:, 0] - timeTotal[0, 0], timeTotal[:, 1])
        # plt.plot(timeTotal[:,0], timeTotal[:,1])
        plt.xlabel('Time (s)', weight='bold')
        plt.ylabel('Energy (W.h)', weight='bold')
        plt.grid()
        plt.legend([host_name])
    else:
        print(f"Estimating power values (1T: {inst_pwr_1T} W and MT: {inst_pwr_MT} W)")

    # Parse all result files:
    for lang in allDict.keys():
        dlang = allDict[lang]

        for variant in dlang.keys():
            dvar = dlang[variant]

            for mode in dvar.keys():
                dres = dvar[mode]  # f / r

                for ii, filename in enumerate(dres[r_f]):
                    # print(f"filename: {filename}")
                    print(f"[{lang}|{variant}|{mode}] Parsing {filename} ...")

                    case_1T = (mode == m_1T)

                    timings = np.zeros(10)
                    n = 0

                    with open(filename) as f:
                        for i, line in enumerate(f):
                            if "BENCHMARK Start" in line:
                                start = int(str.split(line, ':')[-1])
                            if "BENCHMARK Stop" in line:
                                stop = int(str.split(line, ':')[-1])
                            if "|Bench| Time" in line:
                                # parse individual times ? '|Bench| Time (s):'
                                timings[n] = float(str.split(line, ':')[-1])
                                n = n + 1

                    if n == 0:
                        continue;

                    iter_scale = 1.0

                    # fix missing iterations (numpy):
                    if n < 10:
                        # print(f"Missing iterations for: {filename}")
                        iter_scale = (10.0 / n)
                        # print(f"iter_scale: {iter_scale}")

                        # repeat last good value:
                        timing = timings[n - 1]
                        timings[n:10] = timing
                        # print(f"timings: {timings}")

                    # print(f"start: {start} stop: {stop}")
                    # print(f"timings: {timings}")
                    # print(f"timings: mean = {np.mean(timings)} std = {np.std(timings)} median = {np.percentile(timings, 50)}, pct(95%) = {np.percentile(timings, 95)}")

                    # Median percentile and stddev of individual iterations:
                    ind_time_med = np.percentile(timings, 50)
                    ind_time_stddev = np.std(timings)
                    # Total represents a better estimate of wall time:
                    ind_time_total = np.sum(timings)

                    if not load_watt_data:
                        # Missing watt data: mock values from average values (1T or MT)
                        # generate timeTotal values:
                        time_len = (stop - start) + 2
                        # print(f"time_len: {time_len}")
                        # timeTotal[timestamp][0 = Watt, 1 = Power integrated]
                        timeTotal = np.zeros((time_len, 2))
                        # print("timeTotal dims: ", np.shape(timeTotal))

                        inst_pwr_val = inst_pwr_1T if case_1T else inst_pwr_MT

                        for it in range(0, time_len):
                            timeTotal[it, 0] = start + it
                            timeTotal[it, 1] = inst_pwr_val

                        # print(timeTotal)

                    nt = 1 if case_1T else nTh
                    th = ii + 1

                    istart = np.where(timeTotal[:, 0] <= start)[-1][-1]
                    istop = np.where(timeTotal[:, 0] > stop)[0][0]
                    # print("# ", case_1T, ii, filename, " time: ", istart, "to ", istop)

                    rel_time = timeTotal[istart:istop, 0] - timeTotal[istart, 0]
                    wall_time = (timeTotal[istop, 0] - timeTotal[istart, 0]) * iter_scale

                    # walltime is not enough precise, so use ind_time_total instead (total time for 10 iterations)
                    delta_time = wall_time - ind_time_total
                    ratio_energy = 1.0 - (delta_time / ind_time_total)
                    # print(f"wall_time: {wall_time} ind_time_total: {ind_time_total} : delta= {delta_time}")
                    # print(f"ratio_energy: {ratio_energy}")

                    power_inst = timeTotal[istart:istop, 1]
                    power_inst_avg = np.mean(power_inst)
                    energy = np.sum(power_inst) * iter_scale * pwr_dt * ratio_energy / 3600.0  # W.h

                    # Normalize by unit of work:
                    energy_normalized = energy / nt
                    time_normalized = ind_time_total / nt

                    dres[r_r].append({
                        'cpu_type': cpu_type, 'lang': lang, 'variant': variant, 'mode': mode, 'nt': nt, 'th': th,
                        'ind_time_med': ind_time_med, 'ind_time_stddev': ind_time_stddev, 'ind_time_total': ind_time_total,
                        'time_normalized': time_normalized,
                        'power_inst_avg': power_inst_avg, 'energy': energy, 'energy_normalized': energy_normalized,
                        'filename': filename, 'timings': timings
                    })

                    # only show FIRST result:
                    if th != 1:
                        continue

                    label = lang + ' (' + variant + ') [' + str(nt) + 'T]'
                    plt.figure(99)
                    plt.plot(rel_time, power_inst, label=label)
                    plt.grid(True)
                    plt.legend()

                    if case_1T:
                        # Single Core (1T):
                        plt.figure(100)
                        plt.plot(rel_time, power_inst, label=label)
                        plt.grid(True)
                        plt.legend()
                    else:
                        # Multi core (MT):
                        plt.figure(101)
                        plt.plot(rel_time, power_inst, label=label)
                        plt.grid(True)
                        plt.legend()

        # files processed:
        if len(dlang[v_ref][m_1T][r_r]) == 0:
            dlang[v_ref].pop(m_1T)
        if len(dlang[v_ref][m_MT][r_r]) == 0:
            dlang[v_ref].pop(m_MT)
        if len(dlang[v_ref]) == 0:
            dlang.pop(v_ref)
        if len(dlang[v_opt][m_1T][r_r]) == 0:
            dlang[v_opt].pop(m_1T)
        if len(dlang[v_opt][m_MT][r_r]) == 0:
            dlang[v_opt].pop(m_MT)
        if len(dlang[v_opt]) == 0:
            dlang.pop(v_opt)

    # print(f"allDict: {allDict}")

    # plot all timing results as box/violin plots:
    fig, (ax1, ax2) = plt.subplots(2)  # Use 2 subplots to split opt or not
    vp_pos = {'1': [], '2': []}
    vp_lab = {'1': [], '2': []}
    vp_data = {'1': [], '2': []}

    for lang in allDict.keys():
        # print(f"lang: {lang}")

        # skip numpy:
        if 'numpy' in lang:
            continue
        dispLang = lang
        if 'python_' in dispLang:
            dispLang = dispLang[7: len(dispLang)]
            # print(dispLang)

        dlang = allDict[lang]

        # 1T first
        cat = '1'
        for variant in dlang.keys():
            dvar = dlang[variant]

            result = dvar[m_1T][r_r][0]
            data = result['timings']

            vp_pos[cat].append(len(vp_pos[cat]))
            vp_lab[cat].append(f"{dispLang}\n[{variant}]\n1T")
            vp_data[cat].append(data)

        # MT first
        cat = '2'
        for variant in dlang.keys():
            dvar = dlang[variant]

            # merge all MT results:
            data = np.zeros(0)
            for result in dvar[m_MT][r_r]:
                data = np.append(data, result['timings'])

            vp_pos[cat].append(len(vp_pos[cat]))
            vp_lab[cat].append(f"{dispLang}\n[{variant}]\nMT")
            vp_data[cat].append(data)

    # print(f"vp_pos: {vp_pos}")
    # print(f"vp_lab: {vp_lab}")
    # print(f"vp_data: {vp_data}")

    parts = ax1.violinplot(vp_data['1'], vp_pos['1'],
                           points=10, widths=0.7,
                           showmeans=True, showextrema=True, showmedians=True,
                           bw_method=0.5)

    for ii, pc in enumerate(parts['bodies']):
        pc.set_facecolor('tab:blue' if (ii % 2 == 0) else 'tab:green')
        pc.set_edgecolor('black')

    ax1.set_xticks(vp_pos['1'])
    ax1.set_xticklabels(vp_lab['1'])
    ax1.set_ylabel('Time (s)')
    ax1.set_ylim([0.0, time_ylim_1T])
    ax1.set_yticks(np.arange(0.0, time_ylim_1T, step=1.0))
    ax1.grid(visible=True, which='both')

    parts = ax2.violinplot(vp_data['2'], vp_pos['2'],
                           widths=0.7,
                           showmeans=True, showextrema=True, showmedians=True,
                           bw_method=0.5)

    for ii, pc in enumerate(parts['bodies']):
        pc.set_facecolor('tab:blue' if (ii % 2 == 0) else 'tab:green')
        pc.set_edgecolor('black')

    ax2.set_xticks(vp_pos['2'])
    ax2.set_xticklabels(vp_lab['2'])
    ax2.set_ylabel('Time (s)')
    ax2.set_ylim([0.0, time_ylim_MT])
    ax2.set_yticks(np.arange(0.0, time_ylim_MT, step=1.0))
    ax2.grid(visible=True, which='both')

    plt.tight_layout(pad=0.2, w_pad=0.2, h_pad=0.2)


    # Post process (all results):
    # group results:
    results_ref_1T = list()
    results_opt_1T = list()
    results_ref_MT = list()
    results_opt_MT = list()

    # rank ie ratio (lang / C-opt):
    ref_1T = allDict[lang_C][v_opt][m_1T][r_r][0]
    # print(f"ref_1T: {ref_1T}")
    ref_MT = allDict[lang_C][v_opt][m_MT][r_r][0]  # use all results ?
    # print(f"ref_MT: {ref_MT}")

    lang_results = {}

    for lang in allDict.keys():
        dlang = allDict[lang]

        for variant in dlang.keys():
            dvar = dlang[variant]

            for mode in dvar.keys():
                dres = dvar[mode]  # f / r

                case_1T = (mode == m_1T)

                if len(dres[r_r]) > 1:
                    # keep the best MT result = closest to median(ind_time_total)
                    values = np.zeros(len(dres[r_r]))
                    for ii, result in enumerate(dres[r_r]):
                        values[ii] = result['ind_time_total']
                    # print(f"values: {values}")

                    median = np.percentile(values, 50)
                    # print(f"median: {median}")

                    pos = 0
                    min_dist = 10.0
                    for ii, result in enumerate(dres[r_r]):
                        dist = abs(result['ind_time_total'] - median)
                        if dist < min_dist:
                            min_dist = dist
                            pos = ii
                    # keep the median result (no stats)
                    dres[r_r] = [dres[r_r][pos]]
                    # print(f"selected: {dres[r_r]}")

                for result in dres[r_r]:
                    # print(f"result: {result}")

                    ref = ref_1T if case_1T else ref_MT

                    result['score_time_normalized'] = result['time_normalized'] / ref['time_normalized']
                    result['score_energy_normalized'] = result['energy_normalized'] / ref['energy_normalized']

                    # print(f" result keys: {result.keys()}")

                    # group results:
                    case_opt = (variant == v_opt)
                    if case_1T:
                        if case_opt:
                            results_opt_1T.append(result)
                        else:
                            results_ref_1T.append(result)
                    else:
                        if case_opt:
                            results_opt_MT.append(result)
                        else:
                            results_ref_MT.append(result)

                # add result in lang table
                k = f"{mode}-{variant}"
                if not k in lang_results:
                    lang_table = {'cpu_type': cpu_type, 'variant': variant, 'mode': mode}
                    lang_results[k] = lang_table

                # add lang
                result = dres[r_r][0]
                lang_results[k][lang] = {'E': result['energy_normalized'], 'T': result['time_normalized']}

    # Save dictionaries for later processing:
    if False:
        with open(f"./output/{host_name}-all_dict.dic", 'w') as f:
            f.write(str(allDict))

    with open(f"./output/{host_name}-lang.dic", 'w') as f:
        f.write(str(lang_results))

    # Write 1 CSV per lang result:
    for key, result in lang_results.items():
        # print(key, '->', result)
        with open(f"./output/{host_name}-lang-{key}.csv", 'w') as f:
            f.write("###################\n")
            f.write(f"# host: {host_name}\n")
            f.write(f"# variant: {result['variant']}\n")
            f.write(f"# mode: {result['mode']}\n")

            f.write("\n# Normalized Energy (W.h)\n")
            # columns
            f.write("# CPU;")
            for lang in allDict.keys():
                f.write(f"{lang};")
            f.write("\n")
            # data:
            f.write(f"{result['cpu_type']};")
            for lang in allDict.keys():
                if lang in result:
                    f.write("{:.9f};".format(result[lang]['E']))
                else:
                    f.write(";")
            f.write("\n")

            f.write("\n# Normalized Time (s)\n")
            # columns
            f.write("# CPU;")
            for lang in allDict.keys():
                f.write(f"{lang};")
            f.write("\n")
            # data:
            f.write(f"{result['cpu_type']};")
            for lang in allDict.keys():
                if lang in result:
                    f.write("{:.9f};".format(result[lang]['T']))
                else:
                    f.write(";")
            f.write("\n")

    # Dump FULL CSV Table:
    with open(f"./output/{host_name}-table.csv", 'w') as f:
        f.write("###################\n")
        f.write(f"# host: {host_name}\n")
        f.write("# CPU; Variant; Th; Lang;")
        f.write(" Inst. Power; Energy; Normalized Energy;")
        f.write(" Ind. time (med); Ind. time (stddev); Wall time; Normalized time;")
        f.write(" Time ratio; Energy ratio; filename\n")
        f.write("# ;;;;W;W.h;W.h;s;s;s;;;;;\n")
        f.write("# 1T ---\n")
        # REF + 1T:
        for result in results_ref_1T:
            f.write("{};{};{};{};{:.9f};{:.9f};{:.9f};{:.9f};{:.9f};{:.9f};{:.9f};{:.9f};{:.9f};{}\n".format(
                result['cpu_type'], result['variant'], result['nt'], result['lang'],
                result['power_inst_avg'], result['energy'], result['energy_normalized'],
                result['ind_time_med'], result['ind_time_stddev'], result['ind_time_total'], result['time_normalized'],
                result['score_time_normalized'], result['score_energy_normalized'],
                result['filename']))
        # OPT + 1T:
        for result in results_opt_1T:
            f.write("{};{};{};{};{:.9f};{:.9f};{:.9f};{:.9f};{:.9f};{:.9f};{:.9f};{:.9f};{:.9f};{}\n".format(
                result['cpu_type'], result['variant'], result['nt'], result['lang'],
                result['power_inst_avg'], result['energy'], result['energy_normalized'],
                result['ind_time_med'], result['ind_time_stddev'], result['ind_time_total'], result['time_normalized'],
                result['score_time_normalized'], result['score_energy_normalized'],
                result['filename']))
        f.write("# MT ---\n")
        # REF + MT:
        for result in results_ref_MT:
            f.write("{};{};{};{};{:.9f};{:.9f};{:.9f};{:.9f};{:.9f};{:.9f};{:.9f};{:.9f};{:.9f};{}\n".format(
                result['cpu_type'], result['variant'], result['nt'], result['lang'],
                result['power_inst_avg'], result['energy'], result['energy_normalized'],
                result['ind_time_med'], result['ind_time_stddev'], result['ind_time_total'], result['time_normalized'],
                result['score_time_normalized'], result['score_energy_normalized'],
                result['filename']))
        # REF + MT:
        for result in results_opt_MT:
            f.write("{};{};{};{};{:.9f};{:.9f};{:.9f};{:.9f};{:.9f};{:.9f};{:.9f};{:.9f};{:.9f};{}\n".format(
                result['cpu_type'], result['variant'], result['nt'], result['lang'],
                result['power_inst_avg'], result['energy'], result['energy_normalized'],
                result['ind_time_med'], result['ind_time_stddev'], result['ind_time_total'], result['time_normalized'],
                result['score_time_normalized'], result['score_energy_normalized'],
                result['filename']))


    # Dump Latex Table
    if True:
        print("###################")
        print("""
\\begin{tabular}{|l|c|l|}
\\hline
Column & Unit & Comments \\\\
\\hline
Inst. Power & W & average value over the job \\\\
Energy & W.h & integrated power (50 Hz) of the job \\\\
Normalized Time & s & 1 job = 10 repeats / nthread \\\\
Normalized Energy & W.h & reduced power = Integrated Power / jobs \\\\
Time ratio & & Normalized Time compared to (C-opt) \\\\
Energy ratio & & Normalized Energy compared to (C-opt) \\\\
\\hline
\\end{tabular}
""")

        print("\\begin{tabular}{|c|l|c|c|c|c|c|c|c|c|c|}")
        print(f"% host: {host_name}")
        print("\\hline")
        print("CPU & Lang & Th & Inst. Power & Energy & Normalized Time & Energy & Time ratio & Energy ratio \\\\ % filename")
        print("\\hline")
        # REF + 1T:
        for ii, result in enumerate(results_ref_1T):
            print("{} & {} & {} & {:.2f} & {:.2f} & {:.2f} & {:.2f} & {:.2f} & {:.2f} \\\\ % {}".format(
                    result['cpu_type'], result['lang'], result['nt'], result['power_inst_avg'], result['energy'],
                    result['time_normalized'], result['energy_normalized'],
                    result['score_time_normalized'], result['score_energy_normalized'],
                    result['filename']))
        print("\\hline")
        # REF + MT:
        for ii, result in enumerate(results_ref_MT):
            print("{} & {} & {} & {:.2f} & {:.2f} & {:.2f} & {:.2f} & {:.2f} & {:.2f} \\\\ % {}".format(
                    result['cpu_type'], result['lang'], result['nt'], result['power_inst_avg'], result['energy'],
                    result['time_normalized'], result['energy_normalized'],
                    result['score_time_normalized'], result['score_energy_normalized'],
                    result['filename']))
        print("\\hline \\hline")
        # REF + 1T:
        for ii, result in enumerate(results_opt_1T):
            print("{} & {} & {} & {:.2f} & {:.2f} & {:.2f} & {:.2f} & {:.2f} & {:.2f} \\\\ % {}".format(
                    result['cpu_type'], result['lang'], result['nt'], result['power_inst_avg'], result['energy'],
                    result['time_normalized'], result['energy_normalized'],
                    result['score_time_normalized'], result['score_energy_normalized'],
                    result['filename']))
        print("\\hline")
        # REF + MT:
        for ii, result in enumerate(results_opt_MT):
            print("{} & {} & {} & {:.2f} & {:.2f} & {:.2f} & {:.2f} & {:.2f} & {:.2f} \\\\ % {}".format(
                    result['cpu_type'], result['lang'], result['nt'], result['power_inst_avg'], result['energy'],
                    result['time_normalized'], result['energy_normalized'],
                    result['score_time_normalized'], result['score_energy_normalized'],
                    result['filename']))
        print("\\hline")
        print("\end{tabular}")


    if False:
        fig, ax = plt.subplots()
        labels = list()
        n = 0
        for result in results_ref_1T:
            if n == 0:
                ax.bar([n - 0.3], result['energy_normalized'], color='b', width=0.2, label="Not optimised")
            else:
                ax.bar(n - 0.3, result['energy_normalized'], color='b', width=0.2)
            labels.append(result['lang'])
            n = n + 1
        n = 0
        for result in results_opt_1T:
            if n == 0:
                ax.bar(n - 0.1, result['energy_normalized'], color='g', width=0.2, label="Optimised")
            else:
                ax.bar(n - 0.1, result['energy_normalized'], color='g', width=0.2)
            n = n + 1
        n = 0
        for result in results_ref_MT:
            if n == 0:
                ax.bar(n + 0.1, result['energy_normalized'], color='r', width=0.2, label="Not optimised multithread")
            else:
                ax.bar(n + 0.1, result['energy_normalized'], color='r', width=0.2)
            n = n + 1
        n = 0
        for result in results_opt_MT:
            if n == 0:
                ax.bar(n + 0.3, result['energy_normalized'], color='k', width=0.2, label="Optimised multithread")
            else:
                ax.bar(n + 0.3, result['energy_normalized'], color='k', width=0.2)
            n = n + 1

        ax.set_ylabel('Power reduced')
        ax.set_title('Power reduced by language')
        ax.set_xticks(np.arange(len(labels)))
        ax.set_xticklabels(labels)
        ax.legend()
        fig.tight_layout()
        plt.grid(True)


    fig, ax = plt.subplots()
    labels = list()
    for ii, result in enumerate(results_ref_1T):
        if ii == 0:
            ax.bar(ii - 0.1, result['energy_normalized'], color='b', width=0.2, label="Not optimised [1T]")
        else:
            ax.bar(ii - 0.1, result['energy_normalized'], color='b', width=0.2)  # , label = "Not optimised")
        labels.append(result['lang'])
    for iii, result in enumerate(results_opt_1T):
        if iii == 0:
            ax.bar(iii + 0.1, result['energy_normalized'], color='g', width=0.2, label="Optimised [1T]")
        else:
            ax.bar(iii + 0.1, result['energy_normalized'], color='g', width=0.2)  # , label = "Optimised")
    ax.set_ylabel('Energy (W.h)', weight='bold')
    ax.set_xticks(np.arange(ii + 1))
    ax.set_xticklabels(labels)
    ax.legend()
    fig.tight_layout()
    plt.grid(True)

    fig, ax = plt.subplots()
    labels = list()
    for ii, result in enumerate(results_ref_MT):
        if ii == 0:
            ax.bar(ii - 0.1, result['energy_normalized'], color='r', width=0.2, label="Not optimised [MT]")
        else:
            ax.bar(ii - 0.1, result['energy_normalized'], color='r', width=0.2)  # , label = "Not optimised")
        labels.append(result['lang'])
    for iii, result in enumerate(results_opt_MT):
        if iii == 0:
            ax.bar(iii + 0.1, result['energy_normalized'], color='k', width=0.2, label="Optimised [MT]")
        else:
            ax.bar(iii + 0.1, result['energy_normalized'], color='k', width=0.2)  # , label = "Optimised")
    plt.ylabel('Energy (W.h)', weight='bold')
    ax.set_xticks(np.arange(ii + 1))
    ax.set_xticklabels(labels)
    ax.legend()
    fig.tight_layout()
    plt.grid(True)

    if show_plot:
        plt.show()
