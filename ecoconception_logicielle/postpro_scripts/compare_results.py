import os

import matplotlib.colors as mcolors
import matplotlib.lines as mlines
import matplotlib.pyplot as plt
import matplotlib.ticker as mticker
import numpy as np

plt.rc('font', size=28)

# define all langs:
# skip 'python_numpy'
all_langs = ['C', 'fortran', 'java', 'julia', 'python_numba', 'python_transonic', 'go', 'rust']
print(f"all_langs: {all_langs}")

host_names = ["troll1", "neowise10", "pyxis-4", "sagittaire-6", "zbook-15", "rpi4"]

legend_names = {
    "troll1": "Xeon Gold 5218 [32C]",
    "neowise10": "AMD EPYC 7642 [48C]",
    "pyxis-4": "ARM X2 99xx [64C]",
    "sagittaire-6": "AMD Opteron 250 [2C]",
    "zbook-15": "HP ZBook 15 G3 [4C]",
    "rpi4": "Raspberry Pi 4 [4C]"
}

host_colors = {
    "troll1": "C0",
    "neowise10": "C1",
    "pyxis-4": "C2",
    "sagittaire-6": "C3",
    "zbook-15": "C4",
    "rpi4": "C5"
}


cmp_mode='opt' # or 'ref'

# result_keys=['1T-ref', 'MT-ref', '1T-opt', 'MT-opt']
result_keys = [f"1T-{cmp_mode}", f"MT-{cmp_mode}"]


# define all variants [ref|opt]:
v_ref = 'ref'
v_opt = 'opt'
all_variants = [v_ref, v_opt]
print(f"all_variants: {all_variants}")

# define all modes [1T|MT]:
m_1T = '1T'
m_MT = 'MT'
all_modes = [v_ref, v_opt]
print(f"all_modes: {all_modes}")

mode_linestyle = {
    m_1T: ':',
    m_MT: '-'
}

lang_markers = {
    'C': {'s': "o", 'sc': 0.6, 'z': 0},
    'fortran': {'s': "^", 'sc': 0.9, 'z': 1},
    'java': {'s': "s", 'sc': 0.5, 'z': 0},
    'julia': {'s': "*", 'sc': 1.2, 'z': 2},
    'python_numpy': {'s': ".", 'sc': 1.0, 'z': 0},
    'python_numba': {'s': "P", 'sc': 0.8, 'z': 3},
    'python_transonic': {'s': "X", 'sc': 0.8, 'z': 4},
    'go': {'s': "D", 'sc': 0.5, 'z': 0},
    'rust': {'s': "H", 'sc': 0.8, 'z': 0}
}
# print(f"lang_markers: {lang_markers}")

loaded_results = {}

# Load process (all results):
for host_name in host_names:
    with open(f"./output/{host_name}-lang.dic", 'r') as f:
        text = f.read()
        loaded_dic = eval(text)
        loaded_results[host_name] = loaded_dic
        loaded_results[host_name]['host_name'] = host_name

# print(f"loaded_results: {loaded_results}")


# Write 1 complete CSV:
if True:
    for result_key in result_keys:
        with open(f"./output/cmp-lang-{result_key}.csv", 'w') as f:
            header = True

            for host_result in loaded_results.values():
                result = host_result[result_key]
                host_name = host_result['host_name']

                if header:
                    header = False
                    f.write("###################\n")
                    f.write(f"# key: {result_key}\n")
                    f.write(f"# variant: {result['variant']}\n")
                    f.write(f"# mode: {result['mode']}\n")

                    f.write("\n# E = Normalized Energy (W.h); T = Normalized Time (s):\n")
                    # columns
                    f.write("# CPU;")
                    for lang in all_langs:
                        f.write(f"E {lang};T {lang};")
                    f.write("\n")
                # data:
                f.write(f"{legend_names[host_name]};")
                for lang in all_langs:
                    if lang in result:
                        f.write("{:.3f};".format(result[lang]['E']))
                        f.write("{:.3f};".format(result[lang]['T']))
                    else:
                        f.write(";;")
                f.write("\n")


fig = plt.figure(1, figsize=[25, 15], dpi=100)
axes = plt.axes()
axes.set_xlabel('Wall time (s)')
axes.set_xlim(5e-1, 200)
axes.set_ylabel('Energy (W.h)')
axes.set_ylim(5e-2, 10)

axes.set_xscale("log")
axes.set_yscale("log")

axes.get_xaxis().set_major_formatter(mticker.ScalarFormatter())
axes.get_yaxis().set_major_formatter(mticker.ScalarFormatter())

n = 0
first_lang = True

for host_result in loaded_results.values():
    host_name = host_result['host_name']
    host_color = host_colors[host_name]
    
    for result_key in result_keys:
        result = host_result[result_key]
        langs = []
        times = []
        energies = []

        # print(f"result: {result}")

        for lang in all_langs:
            langs.append(lang)
            if lang in result:
                r = result[lang]
                times.append(r['T'])
                energies.append(r['E'])
            else:
                times.append(np.NAN)
                energies.append(np.NAN)

        # print(f"T: {langs}")
        # print(f"T: {times}")
        # print(f"E: {energies}")

        for ii, lang in enumerate(langs):
            label = lang if first_lang else None
            plt.scatter(times[ii], energies[ii],
                        marker=lang_markers[lang]['s'], s=300 * lang_markers[lang]['sc'],
                        linewidth=2, color=host_color, edgecolors='black', label=label,
                        zorder=10 + lang_markers[lang]['z'])
        first_lang = False

        # sort for lines:
        zipped_sorted = sorted(zip(times, energies, langs),
                               key=lambda i: i[0] if not np.isnan(i[0]) else float('+inf'))  # sorted by times
        times, energies, langs = map(list, zip(*zipped_sorted))

        # print(f"T: {langs}")
        # print(f"T: {times}")
        # print(f"E: {energies}")

        label = f"{legend_names[host_name]}" if result['mode'] == m_MT else None
        ls = mode_linestyle[result['mode']]

        plt.plot(times, energies, label=label, color=host_color, linewidth=5, linestyle=ls, zorder=0)

    # change color per host
    n = n + 1

mode = 'Optimized' if cmp_mode == 'opt' else 'Reference'

plt.title(f"Energy efficiency [{mode}]", pad=15)
plt.grid(True, which='both')

h, l = axes.get_legend_handles_labels()
first_len=len(host_names)

# host names:
first_legend = axes.legend(h[:first_len], l[:first_len], loc='upper left')
# langs
ax = plt.gca().add_artist(first_legend)
sec_legend = axes.legend(h[first_len:], l[first_len:], loc='center right')
plt.gca().add_artist(sec_legend)

# 1T / MT:
line_1T = mlines.Line2D([], [], color='black', linestyle=mode_linestyle[m_1T], linewidth=5, label='mono-thread [1T]')
line_MT = mlines.Line2D([], [], color='black', linestyle=mode_linestyle[m_MT], linewidth=5, label='multi-thread [MT]')
axes.legend(handles=[line_1T, line_MT], loc='upper center')

plt.subplots_adjust(top=0.95, bottom=0.07, left=0.075, right=0.98)

plt.savefig("ecolang_compare.png", dpi=200)
plt.show()

