#!/bin/bash

# Use US Locale:
export LANG=C

if [ -z "$1" ]
  then
    echo "No argument 1 (CMD) supplied"
    exit 1
fi


processId="1D" # all benchmarks contains '1D'

# Check if service is running:
PID=`ps -e | grep "${processId}" | grep -v "grep" | grep -v "\-bench\.sh" | awk '{print $1}'`
#echo "PID: '${PID}'"

N_PID=`echo "${PID}" | wc -w`

if [ "${N_PID}" -ne "0" ]
then
    echo "Process '${processId}' (PID = ${PID}) is already running ..."
    exit 1
fi


CURRENT=`pwd`
CMD="$1"
DIR=`dirname $CMD`
LABEL=`basename $CMD`

dir=./results/${LABEL}
mkdir -p $dir


echo "---------------------------------------"
echo "Environment on command '$CMD' ..."

ENV_SH=$CURRENT/$DIR/env.sh
if [ -f "$ENV_SH" ]
then
    cd $CURRENT/$DIR
    bash ./env.sh
    cd $CURRENT
fi

echo "---------------------------------------"

