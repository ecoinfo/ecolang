#!/bin/bash

DIR=./results/bourgesl-HP-ZBook-15-G3-2020_09_03/1Djulia/

OUT=/tmp/1T.txt
echo "time" > $OUT
grep -h "Time" $DIR/result_*_single.txt | sed "s/|Bench| Time (s): //g" >> $OUT


OUT_MT=/tmp/4T.txt
echo "time" > $OUT_MT
grep -h "Time" $DIR/result_*_core_*.txt | sed "s/|Bench| Time (s): //g" >> $OUT_MT


python postpro_scripts/process-results.py $OUT $OUT_MT

