#!/bin/bash

DO_ALL=1

# Use US Locale:
export LANG=C

USE_CPU_POWER=1


# Initialize CPU:
if [ "$USE_CPU_POWER" -eq "1" ]
then
    # PROFILE in [cpu_normal | cpu_fixed | cpu_hpc | cpu_hpc_turbo]
    PROFILE=cpu_hpc_turbo

    echo "adjusting CPU (HT OFF + ${PROFILE}) - requires SUDO"
    sudo ./${PROFILE}.sh 2>&1 > results/cpu_profile.txt
    sudo ./cpu_ht_off.sh 2>&1 > /dev/null
fi


# get CPU infos (settings):
source ./bench-settings.sh

if [ -z "$CPU_CORES" ]
then
    CPU_CORES=4 # 4 CPU cores by default (rasberry pi 4 or laptop)
    echo "Using default value for CPU_CORES = $CPU_CORES"
else
    echo "Using CPU_CORES = $CPU_CORES"
fi


# Log host env:
./log_env.sh


# Log timestamps (needed by getwatts)
hostname > results/timestamp


if [ "$DO_ALL" -eq "1" ] ; then

# C
./run-bench.sh cfiles/1DC
./run-bench.sh cfiles/1DC_opt

# Fortran
./run-bench.sh ffiles/1Dfortran 
./run-bench.sh ffiles/1Dfortran_opt 

# Java
./run-bench.sh javafiles/1Djava
./run-bench.sh javafiles/1Djava_opt

# Julia
./run-bench.sh jlfiles/1Djulia
./run-bench.sh jlfiles/1Djulia_opt

# Python
# warning: numpy (1 iteration only)
./run-bench.sh pyfiles/1Dpython_numpy
./run-bench.sh pyfiles/1Dpython_numpy_opt

./run-bench.sh pyfiles/1Dpython_transonic
./run-bench.sh pyfiles/1Dpython_transonic_opt

./run-bench.sh pyfiles/1Dpython_numba
./run-bench.sh pyfiles/1Dpython_numba_opt

fi


# Tests:

# Rust:
./run-bench.sh rustfiles/1Drust
./run-bench.sh rustfiles/1Drust_opt
./run-bench.sh rustfiles/1Drust_opt3

# Go:
./run-bench.sh gofiles/1Dgo
./run-bench.sh gofiles/1Dgo_opt


# Restore CPU:
if [ "$USE_CPU_POWER" -eq "1" ]
then
    echo "adjusting CPU (HT ON + normal) - requires SUDO"
    sudo ./cpu_ht_on.sh 2>&1 > /dev/null
    sudo ./cpu_normal.sh 2>&1 > /dev/null
fi

echo "That's All, folks !!!"

