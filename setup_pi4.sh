#!/bin/bash
#
# Setup for Raspbian 7 (armv7)
#
DOCKER=0

# Setup script
# set -eux to fail fast
set -eux ;

BASE_PKG="procps psmisc vim wget curl linux-cpupower"
BUILD_PKG="make gcc g++ gfortran llvm python3 python3-dev python3-pip"
apt-get update
apt-get install -y --no-install-recommends $BASE_PKG $BUILD_PKG


# Python:
pip3 install --upgrade pip setuptools wheel # separate install needed
pip3 install numpy
pip3 install transonic pythran
pip3 install numba matplotlib
pip3 install requests execo


# Julia
JL_VER=1.7.1
JL_ARCH=armv7l
JL_DIR=armv7l
JL=julia-${JL_VER}-linux-${JL_ARCH}.tar.gz
wget --no-check-certificate -O $JL https://julialang-s3.julialang.org/bin/linux/${JL_DIR}/1.7/${JL}
tar xvfz $JL && rm $JL && rm -rf /opt/julia-* && mv julia-${JL_VER} /opt/
ln -s /opt/julia-${JL_VER} /opt/julia
echo "Julia installed at /opt/julia-${JL_VER}/"


# Rust
RUST_VER=1.57.0
RUST_ARCH=armv7
RUST=rust-${RUST_VER}-${RUST_ARCH}-unknown-linux-gnueabihf
RUST_DL=$RUST.tar.gz
wget --no-check-certificate -O $RUST_DL https://static.rust-lang.org/dist/${RUST_DL}
tar xvfz $RUST_DL && rm $RUST_DL && cd $RUST && ./install.sh --destdir=/opt/rust-${RUST_VER} --prefix= && cd .. && rm -rf $RUST
ln -s /opt/rust-${RUST_VER} /opt/rust
echo "Rust installed at /opt/rust-${RUST_VER}"


# Go
GO_VER=1.17.5
GO_ARCH=armv6l
GO=go${GO_VER}.linux-${GO_ARCH}
GO_DL=$GO.tar.gz
wget --no-check-certificate -O $GO_DL https://dl.google.com/go/${GO_DL}
tar xvfz $GO_DL && rm $GO_DL && mv go go-${GO_VER} && mv go-${GO_VER} /opt/
ln -s /opt/go-${GO_VER} /opt/go
echo "Go installed at /opt/go-${GO_VER}"


# Docker:
if [ "$DOCKER" -eq "1" ]; then
    apt-get remove docker docker-engine docker.io containerd runc
    apt-get install -y \
        apt-transport-https \
        ca-certificates \
        curl \
        gnupg-agent \
        software-properties-common
    curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add -
    add-apt-repository \
       "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
       $(lsb_release -cs) \
       stable"
    apt-get update
    apt-get install -y docker-ce docker-ce-cli containerd.io
fi

echo "setup done."

